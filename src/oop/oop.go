package main

import (
	"fmt"
)

type Human struct {
	Name     string
	CanThink bool
}

type SuperHuman struct {
	Human // this is composition, not inheritance
	Power uint16
}

func main() {

	// struct literal, notice the variable name, utf-8 power
	toño := Human{
		Name:     "toño",
		CanThink: true,
	}
	supertoño := SuperHuman{
		Human: Human{
			Name:     "Supertoño",
			CanThink: true,
		},
		Power: 9001,
	}

	jose := new(Human)

	fmt.Println("Jose ", jose)
	fmt.Println("toño ", toño)
	fmt.Println("Supertoño ", supertoño)

	// notice how I access to CanThink member
	fmt.Println("Supertoño can think? ", supertoño.CanThink)

}
