package main

import "fmt"

func main() {
	fmt.Println("At your command")

	var d string
	fmt.Scanf("%s", &d)

	switch d {
	case "n":
		fmt.Println("Go to the north")
		break
	case "s":
		fmt.Println("Go to the south")
		break
	case "e":
		fmt.Println("Go to the east")
		break
	case "w":
		fmt.Println("Go to the west")
		break
	}

}
